<?php

namespace Drupal\custom_site\Form;

use Drupal\Component\Utility\Environment;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormBase;

class CustomImportNodeForm extends FormBase {

  /**
   * Batch Builder.
   *
   * @var \Drupal\Core\Batch\BatchBuilder
   */
  protected BatchBuilder $batchBuilder;

  public function getFormId() {
    return 'custom_import_node_form';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $validators = [
      'file_validate_extensions' => 'csv',
      'file_validate_size' => Environment::getUploadMaxSize(),
    ];

    $form['file'] = [
      '#type' => 'file',
      '#title' => t('File to import'),
      '#description' => [
        '#theme' => 'file_upload_help',
        '#upload_validators' => $validators,
      ],
      '#upload_validators' => $validators,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
    ];

    return $form;
  }

  public function validateForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if (!is_dir('public://import_nodes')) {
      mkdir('public://import_nodes');
    }

    $this->file = _file_save_upload_from_form($form['file'], $form_state, 0);

    // Ensure we have the file uploaded.
    if (!$this->file) {
      $form_state->setErrorByName('file', $this->t('File not found.'));
    }
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $file_handle = fopen($this->file->getFileUri(), 'r');
    $this->batchBuilder = new BatchBuilder();
    $this->batchBuilder
      ->setTitle($this->t('Processing'))
      ->setInitMessage($this->t('Initializing.'))
      ->setProgressMessage($this->t('Completed @current of @total.'))
      ->setErrorMessage($this->t('An error has occurred.'));
    $this->batchBuilder->setFile(drupal_get_path('module', 'custom') . '/src/Form/BatchForm.php');
    $this->batchBuilder->setFinishCallback([$this, 'finished']);

    $items = [];
    $headers = [];
    for ($i = 0; $i < 1000; $i++) {
      $csv_line = fgets($file_handle);
      $csv_line = trim($csv_line);
      $csv_line = str_getcsv($csv_line);
      if (is_array($csv_line)) {
        if (!empty($headers) && !empty($csv_line[0])) {
          $items[] = $csv_line;
        } else {
          $headers = $csv_line;
        }
      }
    }

    $sets = array_chunk($items, 50);
    foreach ($sets as $set) {
      $this->batchBuilder->addOperation([$this, 'processItems'], [$set, count($items)]);
    }

    batch_set($this->batchBuilder->toArray());
  }

  /**
   * {@inheritdoc}
   */
  public function finished($success, $results, $operations) {
    $channel = 'import_nodes';
    if ($success) {
      $message = \Drupal::translation()->formatPlural($results['entities_updated'], 'One entity updated.', '@count entities updated.');

      \Drupal::logger($channel)->info($message);
      \Drupal::service('messenger')->addMessage($message);
    } else {
      $message = t('Import finished with an error.');
      \Drupal::logger($channel)->error($message);
      \Drupal::service('messenger')->addError($message);
    }
  }

  /**
   * Processor for batch operations.
   */
  public function processItems($items, $max, array &$context) {
    // Set default progress values.
    if (empty($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = $max;
    }
    if (empty($context['results']['entities_updated'])) {
      $context['results']['entities_updated'] = 0;
    }

    // Elements per operation.
    foreach ($items as $item) {
      $this->processItem($item, $context);
      $context['results']['entities_updated']++;
      $context['sandbox']['progress']++;
    }

    // If not finished all tasks, we count percentage of process. 1 = 100%.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Process single item.
   */
  public function processItem($item, &$context) {
    $node = \Drupal::entityTypeManager()->getStorage('node')->load($item[0]);
    unset($item[0]);

    $fields = [
      'id',
      'title',
      'status',
      'uid',
      'body',
      'path',
    ];

    if (!empty($node)) {
      foreach ($fields as $key => $field) {
        if (!empty($item[$key])) {
          $node->set($field, $item[$key]);
        }
      }

      $node->save();
    }
  }

}
