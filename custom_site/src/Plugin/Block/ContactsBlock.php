<?php

namespace Drupal\custom_site\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Class ContactsBlock
 *
 * @Block(
 *   id = "contacts_block",
 *   admin_label = @Translation("Contacts Block"),
 *   category = "Custom",
 * )
 */
class ContactsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $contacts = custom_site_get_contacts_data();
    $columns = array_chunk($contacts, ceil(count($contacts) / 3));

    return $columns;
  }

}
