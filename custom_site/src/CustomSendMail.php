<?php

namespace Drupal\custom_site;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class CustomSendMail
 *
 * @package Drupal\custom_site
 */
class CustomSendMail {

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected MailManagerInterface $mailManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Constructs a new CustomSendMail object.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   The mail manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language default.
   */
  public function __construct(MailManagerInterface $mailManager, LanguageManagerInterface $languageManager) {
    $this->mailManager = $mailManager;
    $this->languageManager = $languageManager;
  }

  /**
   * Send email message.
   *
   * @param string $addressTo
   *   The email address where the letter will be sent to.
   * @param TranslatableMarkup $subject
   *   The message subject.
   * @param array $body
   *   A rendered message body.
   * @param array $params
   *   Email parameters.
   *
   * @return bool
   *   TRUE if the message was sent and FALSE otherwise.
   */
  public function sendMail(string $addressTo, TranslatableMarkup $subject, array $body, array $params = []): bool {
    $defaultParams = [
      'headers' => [
        'Content-Type' => 'text/html; charset=UTF-8;',
        'Content-Transfer-Encoding' => '8Bit',
      ],
      'id' => 'mail',
      'reply-to' => NULL,
      'subject' => $subject,
      'langcode' => $this->languageManager->getCurrentLanguage()->getId(),
      'body' => $body,
    ];

    $params = array_replace($defaultParams, $params);
    $message = $this->mailManager->mail('custom_site', $params['id'], $addressTo, $params['langcode'], $params, $params['reply-to']);

    return (bool)$message['result'];
  }
}
